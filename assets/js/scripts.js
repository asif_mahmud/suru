/*---------------------------------------------
Template name:  Suru
Version:        1.0
Author:         Redraw
Author url:    

NOTE:
------
Please DO NOT EDIT THIS JS, you may need to use "custom.js" file for writing your custom js.
We may release future updates so it will overwrite this file. it's better and safer to use "custom.js".

[Table of Content]

01: Main menu
02: Background image
03: Parsley form validation
04: Smooth scroll for comment reply
05: Back to top button
07: Changing svg color
08: Ajax Contact Form

----------------------------------------------*/
    (function () {
        "use strict";
        
    // line in body
        let newDiv =  document.createElement('div');
        newDiv.className = 'line1';
        document.body.appendChild(newDiv);
        let newDiv2 = document.createElement('div2');
        newDiv2.className = 'line2';
        document.body.appendChild(newDiv2);

    })();
    
    (function($) {
        "use strict";

    // menu
    $('.header__menu--items').find('ul li').parents('ul li').addClass('hasSubMenu');
    
    // testimonial carousel
        $('.testimonial--carousel').owlCarousel({
            loop: true,
            margin: 29,
            dots:false,
            nav: true,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });

        // menu js
        $(window).on('scroll', function(){
            if ($(window).scrollTop() < $('.header').height()){
                $('.header__menu--button').parent().removeClass('active')
                $('.header__menu--button').parents('header').removeClass('bg');
                $('.header').removeClass('isActive');
            }else{
                $('.header').addClass('isActive');
            }
            
        });
        
        $('.header__menu--button').on('click', function()  {
            $(this).parent().toggleClass('active');
            $(this).parents('header').toggleClass('bg');
        });

        // Mouse Move

        $('.blog__video').on('mouseenter', function(){
            $(this).mousemove(function (e) {
                let a = $(this).offset().top - $(window).scrollTop()
                let b = $(this).offset().left - $(window).scrollLeft()
                let c = Math.round(e.clientX - b)
                let d = Math.round(e.clientY - a)
                let i = $(this).find('.single__blog--video');
                i.css({
                    left: c + "px",
                    top: d + "px"
                });
            });
        });
        $('.blog__video').on('mouseout', function(){
            $('.single__blog--video').css({
                left: '50%',
                top: '50%'
            });
        });
        


        // Single Blog Slider
        $('.works__carousel').owlCarousel({
            loop: true,
            margin: 0,
            nav: true,
            dots: false,
            
            navText: ['<img class="svg" src="assets/img/icons/arrow-left-long.svg">', '<img class="svg" src="assets/img/icons/arrow-right-long.svg">'],

            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }         
        });

       

        // Single works Slider
        $('.blog__slider--carousel').owlCarousel({
            loop: true,
            margin: 0,
            nav: true,
            dots: false,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            onInitialized: counter, //When the plugin has initialized.
            onTranslated: counter,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }         
        });
        function counter(event) {

            var element = event.target;       
            var items = event.item.count;     
            var item = event.item.index + 1;

            // it loop is true then reset counter from 1
            if (item > items) {
                item = item - items
            }
            $('#counter').html(item);
            $('#total-counter').html("/0" + items)
            console.log(item)
        }
            /* 02: Background image
            ==============================================*/

            var bgImg = $('[data-bg-img]');

            bgImg.css('background', function(){
                return 'url(' + $(this).data('bg-img') + ') center center';
            });


            /* 03: Parsley form validation
            ==============================================*/

            // $('.parsley-validate, .parsley-validate form').parsley();


            /* 04: Smooth scroll for comment reply
            ==============================================*/
            
            var $commentContent = $('.comment-content > a');
            
            $commentContent.on('click', function(event){
                event.preventDefault();
                var $target = $('.comment-form');
                
                if ( $target.length ) {
                    $('html, body').animate({
                        scrollTop: $target.offset().top - 120
                    }, 500);

                    $target.find('textarea').focus();
                }
            });


        /*============================================
            05: Back to top button
        ==============================================*/

        var $backToTopBtn = $('.back-to-top');

        if ($backToTopBtn.length) {
            var scrollTrigger = 400, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $backToTopBtn.addClass('show');
                } else {
                    $backToTopBtn.removeClass('show');
                }
            };

            backToTop();

            $(window).on('scroll', function () {
                backToTop();
            });

            $backToTopBtn.on('click', function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            });
        }

    /*============================================
        07: Changing svg color
    ==============================================*/

        jQuery('img.svg').each(function(){
            var $img = jQuery(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');
        
            jQuery.get(imgURL, function(data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');
        
                // Add replaced image's ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }
        
                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');
                
                // Check if the viewport is set, else we gonna set it if we can.
                if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                    $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
                }
        
                // Replace image with new SVG
                $img.replaceWith($svg);
        
            }, 'xml');
        });

        /*=============================================
         08: Ajax Contact Form
        ==============================================*/

         $('.address-form-inner').on('submit', 'form', function(e) {
            e.preventDefault();

            var $el = $(this);

            $.post($el.attr('action'), $el.serialize(), function(res){
                res = $.parseJSON( res );
                $el.parent('.contact-page-form').find('.form-response').html('<span>' + res[1] + '</span>');
            });
        });

    
    /*==============================================
        10: Preloader
    ==============================================*/

    $(window).on('load', function(){

        function removePreloader() {
            var preLoader = $('.preLoader');
            preLoader.fadeOut();
        }
        setTimeout(removePreloader, 250);
    });



        /*===================================================
                12: Google map
        ===================================================*/
        //Google Map
        if ($('#map').length !== 0) {

            var googleMapSelector = $('#map');
            var latitude = $('.google__map').attr('data-latitude');
            var longitude = $('.google__map').attr('data-longitude');
            var zoome = $('.google__map').attr('data-zoom');
            var zoomtoNum = Number(zoome);
            var mapmarker = $('.google__map').attr('data-marker');
            var myCenter = new google.maps.LatLng(latitude, longitude);

            function initialize() {
                var mapProp = {
                    center: myCenter,
                    zoom: zoomtoNum,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    styles: [
                        {
                            "featureType": "all",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "weight": "2.00"
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#9c9c9c"
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.text",
                            "stylers": [
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#f2f2f2"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape.man_made",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "saturation": -100
                                },
                                {
                                    "lightness": 45
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#eeeeee"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#7b7b7b"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#46bcec"
                                },
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#f3ebdd"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#070707"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        }
                    ]
                };
                var map = new google.maps.Map(document.getElementById('map'), mapProp);
                
                var marker = new google.maps.Marker({
                    position: myCenter,
                    map: map,
                    icon: " ",
                    label: {
                        fontFamily: 'Fontawesome',
                        text: '\uf111'
                    },
                    
                });

            }
            if (googleMapSelector.length) {
                google.maps.event.addDomListener(window, 'load', initialize);
            }
        }   

    
        
         /*=========================================================
        31: counter up
    =========================================================*/
        if ($('.counter').length !==0){
            $('.counter').counterUp({delay:20});
        }
        
    // text splet
        $('figcaption a').each(function () {
            // $(this).html($(this).text().replace(/\s/g, " "));
            // $(this).html($(this).text().replace(/\w*\s/g, "<span class='word'>$&</span>"));
        }); 


        if ($('.skill-circle').length !==0){
            $('.skill-circle').circleProgress({
                size: 65,
                emptyFill: "#fbfbfb"
            });
        }
        


})(jQuery);
